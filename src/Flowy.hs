module Flowy where

import Data.Function ((&))

------------
-- Piping --
------------
infixr 1 <|

(<|) :: (a -> b) -> a -> b
(<|) = ($)

infixl 1 |>

(|>) :: b -> (b -> c) -> c
(|>) = (&)

--------------------------
-- Function Composition --
--------------------------
infixl 1 .>

(<.) :: (b -> c) -> (a -> b) -> a -> c
(<.) = (.)

infixr 1 <.

(.>) :: (a -> b) -> (b -> c) -> a -> c
(.>) = flip (.)

--------------------
-- Functor Piping --
--------------------
infixr 1 <$|

(<$|) :: Functor f => (a -> b) -> (f a -> f b)
(<$|) = fmap

infixr 1 |$>

(|$>) :: Functor f => f a -> (a -> b) -> f b
(|$>) = flip fmap

---------------------------
-- Double Functor piping --
---------------------------
infixr 1 <$$|

(<$$|) :: (Functor f, Functor g) => (a -> b) -> (f (g a) -> f (g b))
(<$$|) = fmap . fmap

infixr 1 |$$>

(|$$>) :: (Functor f, Functor g) => f (g a) -> (a -> b) -> f (g b)
(|$$>) = flip (fmap . fmap)

------------------------
-- Applicative Piping --
------------------------
infixr 1 <*|

(<*|) :: Applicative f => f (a -> b) -> (f a -> f b)
(<*|) = (<*>)

infixr 1 |*>

(|*>) :: Applicative f => f a -> f (a -> b) -> f b
(|*>) = flip (<*>)