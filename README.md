# Flowy
Another lib for writing more understandable haskell! This package, in the same spirit as [Flow],
provides function piping operators that are a bit more intuitive than symbols like `$` and `&`.

## Installation
To add Flowy as a dependency to your package, add it to your Cabal file's `build-depends` section.
```
build-depends: flowy
```

Since this isn't currently on hackage, you'll also need to create a `cabal.project` file with
the following entry:

```
source-repository-package
  type: git
  location: https://gitlab.com/tomorrowstalent/haskell/flowy
```
See [Cabal Reference Docs] for an example of a basic `cabal.project` file using git packages.

[Cabal Reference Docs]: https://cabal.readthedocs.io/en/3.4/cabal-project.html#specifying-packages-from-remote-version-control-locations
